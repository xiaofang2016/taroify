const menus = [
  {
    title: "开发指南",
    children: [
      {
        title: "介绍",
        to: "/introduce/",
      },
      {
        title: "快速上手",
        to: "/quickstart/",
      },
      {
        title: "开发指南",
        to: "/contribution/",
      },
      {
        title: "更新日志",
        to: "/changelog/",
      },
    ],
  },
  {
    title: "基础组件",
    children: [
      {
        title: "Button 按钮",
        to: "/components/button/",
      },
      {
        title: "Cell 单元格",
        to: "/components/cell/",
      },
      {
        title: "Icon 图标",
        to: "/components/icon/",
      },
      {
        title: "Image 图片",
        to: "/components/image/",
      },
      {
        title: "Layout 布局",
        to: "/components/layout/",
      },
      {
        title: "Popup 弹出层",
        to: "/components/popup/",
      },
      {
        title: "Style 内置样式",
        to: "/components/style/",
      },
      {
        title: "Toast 轻提示",
        to: "/components/toast/",
      },
    ],
  },
  {
    title: "反馈组件",
    children: [
      {
        title: "ActionSheet 动作面板",
        to: "/components/action-sheet/",
      },
      {
        title: "Dialog 弹出框",
        to: "/components/dialog/",
      },
      {
        title: "Loading 加载",
        to: "/components/loading/",
      },
      {
        title: "Notify 消息提示",
        to: "/components/notify/",
      },
      {
        title: "Backdrop 背景板",
        to: "/components/backdrop/",
      },
      {
        title: "ShareSheet 分享面板",
        to: "/components/share-sheet/",
      },
    ],
  },
  {
    title: "展示组件",
    children: [
      {
        title: "Badge 徽标",
        to: "/components/badge/",
      },
      {
        title: "Divider 分割线",
        to: "/components/divider/",
      },
      {
        title: "Empty 空状态",
        to: "/components/empty/",
      },
      {
        title: "NoticeBar 通知栏",
        to: "/components/notice-bar/",
      },
      {
        title: "Progress 进度条",
        to: "/components/progress/",
      },
      {
        title: "Skeleton 骨架屏",
        to: "/components/skeleton/",
      },
      {
        title: "Sticky 粘性布局",
        to: "/components/sticky/",
      },
      {
        title: "Swiper 轮播",
        to: "/components/swiper/",
      },
      {
        title: "Tag 标签",
        to: "/components/tag/",
      },
    ],
  },
  {
    title: "导航组件",
    children: [
      {
        title: "Grid 宫格",
        to: "/components/grid/",
      },
      {
        title: "Navbar 导航栏",
        to: "/components/navbar/",
      },
      {
        title: "Pagination 分页",
        to: "/components/pagination/",
      },
      {
        title: "Sidebar 侧边导航",
        to: "/components/sidebar/",
      },
      {
        title: "Tabs 标签页",
        name: "Tabs",
        to: "/components/tabs/",
      },
      {
        title: "Tabbar 标签栏",
        to: "/components/tabbar/",
      },
    ],
  },
]

export default menus
