import Swiper from "./swiper"
import SwiperIndicator from "./swiper-indicator"
import SwiperItem from "./swiper-item"

Swiper.Item = SwiperItem
Swiper.Indicator = SwiperIndicator

export default Swiper
