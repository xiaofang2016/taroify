import { View } from "@tarojs/components"
import { ITouchEvent } from "@tarojs/components/types/common"
import classNames from "classnames"
import * as React from "react"
import { ReactNode } from "react"
import { prefixClassname } from "../styles"

export enum CellSize {
  Medium = "medium",
  Large = "large",
}

type CellSizeString = "medium" | "large"

enum CellAlign {
  Start = "start",
  Center = "center",
  End = "end",
}

type CellAlignString = "start" | "center" | "end"

interface CellProps {
  className?: string
  size?: CellSize | CellSizeString
  align?: CellAlign | CellAlignString
  title?: ReactNode
  brief?: ReactNode
  startIcon?: ReactNode
  endIcon?: ReactNode
  bordered?: boolean
  clickable?: boolean
  children?: ReactNode
  onClick?: (event: ITouchEvent) => void
}

function Cell(props: CellProps) {
  const {
    className,
    size = CellSize.Medium,
    align,
    title,
    brief,
    clickable = false,
    bordered = true,
    startIcon,
    endIcon,
    children,
    onClick,
  } = props

  return (
    <View
      className={classNames(
        prefixClassname("cell"),
        {
          [prefixClassname("cell--start")]: align === CellAlign.Start,
          [prefixClassname("cell--center")]: align === CellAlign.Center,
          [prefixClassname("cell--end")]: align === CellAlign.End,
          [prefixClassname("cell--large")]: size === CellSize.Large,
          [prefixClassname("cell--clickable")]: clickable,
          [prefixClassname("cell--borderless")]: !bordered,
        },
        className,
      )}
      onClick={onClick}
    >
      {startIcon && <View className={prefixClassname("cell-start-icon")}>{startIcon}</View>}
      {title && (
        <View className={prefixClassname("cell__title")}>
          {title}
          {brief && <View className={prefixClassname("cell__brief")} children={brief} />}
        </View>
      )}
      <View
        className={classNames(prefixClassname("cell__value"), {
          [prefixClassname("cell__value--alone")]: !title,
        })}
      >
        {children}
      </View>
      {endIcon && <View className={prefixClassname("cell__end-icon")}>{endIcon}</View>}
    </View>
  )
}

namespace Cell {
  interface GroupProps {
    title?: string
    bordered?: boolean
    children?: ReactNode
  }

  export function Group(props: GroupProps) {
    const { title, bordered = true, children } = props
    return (
      <>
        <View className={prefixClassname("cell-group__title")} children={title} />
        <View
          className={classNames(prefixClassname("cell-group"), {
            [prefixClassname("hairline--top-bottom")]: bordered,
          })}
          children={children}
        />
      </>
    )
  }
}

export default Cell
